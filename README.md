# Netlist Unit Testing framework

Ce projet est là pour faciliter la création d'un simulateur Netlist.

Il est constitué d'une architecture de tests et d'un ensemble de fichiers
Netlist de test progressifs pour tester chaque fonctionalité à implémenter.

### Petit Guide d'Unit Testing

L'idée est simple : découper un programme en petites unités indépendantes,
et les tester pour s'assurer qu'elles fonctionnent avant de les assembler
en un programme plus complexe.

Pour faire court, voilà le scénario :
- Vous implémentez la fonctionalité A
- Vous testez A : tout fonctionne
- Vous implémentez B
- Vous testez B : tout fonctionne
- Vous modifiez A
- Vous êtes obligés de re-tester A et B en entier !
- Vous implémentez C
- Vous modifiez A
- Vous devez re-re-tester ... Bref, vous abandonnez,
  qui a besoin d'une nappe de fils de toute façon ?

Pour éviter ça, on utilise une structure de tests.

Après chaque modification, un programme re-teste automatiquement toutes les
fonctionnalités précédentes, plus celles que vous venez d'implémenter.

Si vous avez cassé quelquechose, il vous indique immédiatement quoi, ainsi
que le fichier netlist et l'entrée à utiliser pour reproduire le bug à la main
et pouvoir le corriger

Si vous êtes intéressé, lisez la suite pour l'installer

## Installation

- Ouvrez un shell / une fenêtre de commande (Ctrl+Alt+t sous Ubuntu)
- Naviguez vers votre dossier netlist
  ```
  cd minijazz/src/netlist
  ```
- Clonez le projet avec la commande suivante :
  ```
  git clone https://git.eleves.ens.fr/drobin01/netlist-autotest.git autotest
  ```
- Lancez le script de configuration :
  ```
  autotest/configure
  ```
- C'est bon !

## Utilisation

Pour lancer les tests, utilisez la commande
```
rake test
```

Vous pouvez configurer les tests que vous souhaitez lancer
dans le fichier **autotest/config.rb**

Quand vous lancez les tests, plein d'infos vont s'afficher, mais tout est
assez facile à lire : il suffit de regarder ce qui est en couleur.

Si tout est vert, vous avez passé tous les tests.

Sinon, le nom du/des test(s) que vous n'avez pas passé(s) sont en rouge,
et une ligne commencant par "FAILURE" vous indique quelles étaient les entrées
(sous la forme d'une liste), ainsi que ce que votre programme a répondu, et ce
qu'il aurait dû répondre. Le fichier netlist correspondant est disponible dans
le dossier autotest.

Si vous modifiez votre code, vous pouvez tout rester en relançant juste **rake test**

## FAQ

### Les tests ne terminent pas

Les tests utilisent l'argument "-n", pour spécifier le nombre de boucles à effectuer.
Vous devez implémenter cette fonctionnalité pour pouvoir utiliser les tests.

Si vous l'avez fait et qu'ils continuent de tourner en boucle, c'est que votre
programme (ou l'original) ne termine pas

### Le fichier que je génère ne s'appelle pas "netlist\_simulator.byte"

Vous pouvez indiquer le nom de votre fichier dans **autotest/config.rb**,
en modifiant la variable test\_file

### J'ai implémenté les registres / nappes de fils, il y a des tests pour ça ?

Oui, mais ils sont désactivés par défaut. Vous pouvez les activer en enlevant
le # devant la ligne concernée dans **autotest/config.rb**

### J'ai implémenté la ROM / RAM, mais je ne vois pas de tests

Il n'y en a pas encore, revenez dans une semaine ;)

### Je n'arrive pas à lancer les tests, il me dit que "ruby" n'existe pas

```
sudo apt-get install ruby
```

### Je n'arrive toujours pas à lancer les tests, il me parle de "minitest" ou "open3"

Le code repose sur deux "gemmes" ruby, des genres de plugins. Il faut les installer

```
gem install minitest open3
```

## Licence

Tout le projet est sous licence MIT - voir [LICENSE](LICENSE) pour plus d'informations

## Auteurs

- **David ROBIN** - Initial framework and tests
