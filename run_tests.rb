#!/usr/bin/env ruby

require_relative "config.rb"
require "test/unit"
require "open3"

class Integer
  def as_array(format)
    n = self
    b = [ n % 2 ]
    b << n % 2 while (n >>= 1) > 0
    b += [0] * (format.reduce(0,:+) - b.size)
    r = []
    format.each { |i| r << b.shift(i).join }
    r
  end
end

class TestSimulator < Test::Unit::TestCase
  @@test_files = {
    # Fils seuls
    "constant_assignement.net": {input: [],        n: 1 },
    "variable_assignement.net": {input: [1, 1],    n: 1 },
    "binop.net":                {input: [1, 1],    n: 1 },
    "reg.net":                  {input: [],        n: 5 },

    # Nappes de fils
    "multiple_assignement.net": {input: [8, 8],    n: 1 },
    "multiple_binop.net":       {input: [4, 4],    n: 1 },
    "multiple_reg.net":         {input: [],        n: 5 },
    "select.net":               {input: [4],       n: 1 },
    "concat.net":               {input: [8, 4],    n: 1 },
    "slice.net":                {input: [16],      n: 1 },
    "mux.net":                  {input: [1, 1, 1], n: 1 },

    # ROM et RAM

    # Programmes fournis dans tp1
    "cm2.net":                  {input: [1],       n: 5 },
    "nadder.net":               {input: [1, 1],    n: 1 },
    "clock_div.net":            {input: [],        n: 5 },
    "fulladder.net":            {input: [1, 1, 1], n: 1 },
  }
  @@original = $reference_file
  @@yours = $test_file

  def capture_output(command, input=nil)
    output = ""
    Open3.popen3(command) do |stdin, stdout, stderr|
      input.each {|c| stdin.puts c } unless input.nil?
      output = stdout.read.chomp
    end
    res = {}
    output.split("\n").each do |line|
      m = /\s*(\w+)\s*[:=]\s*([01]+)\s*/.match(line)
      res[m[1]] = m[2] if m
    end
    res
  end

  def format_cardinal(format)
    format.reduce(1) { |r,i| r*(1<<i) }
  end

  def full_input(input_format)
    inputs = []
    format_cardinal(input_format).times do |n|
      inputs << n.as_array(input_format)
    end
    inputs
  end

  def random_input(input_format)
    n = format_cardinal(input_format)
    (0...$input_randomization_threshold).map { rand(n).as_array(input_format) }
  end

  def generate_inputs(input_file, n)
    format = @@test_files[input_file.to_sym][:input] * n
    return [] if format.empty?
    if format_cardinal(format) <= $input_randomization_threshold
      full_input(format)
    else
      random_input(format)
    end
  end

  def compare_outputs(original_file, new_file, input_file)
    n = @@test_files[input_file.to_sym][:n]
    generate_inputs(input_file, n).each do |input|
      assert_equal(capture_output("./#{original_file} -n #{n} autotest/#{input_file}", input),
                   capture_output("./#{new_file} -n #{n} autotest/#{input_file}", input),
                   "FAILURE with input #{input.inspect}")
    end
  end

  $tests_to_perform.each do |file|
    define_method :"test_#{file}" do
      compare_outputs(@@original, @@yours, "#{file}.net")
    end
  end
end
