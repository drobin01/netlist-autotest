$reference_file = "netlist_simulator-4.03.0.byte"

$test_file = "netlist_simulator.byte"

# ----- INSTRUCTIONS -----
# Vous pouvez  désactiver un test en ajoutant un "#" devant son nom
# Enlevez le "#" pour activer le test

$tests_to_perform = [
  # --- Fils seuls ---
  "constant_assignement",
  "variable_assignement",
  "binop",
# "reg",

  # --- Nappes de fils ---
# "multiple_assignement",
# "multiple_reg",
# "select",
# "concat",
# "slice",
# "mux",

# This one does not work on original simulator, will always fail test
# "multiple_binop",

  # --- ROM et RAM ---
  # TODO : ROM and RAM test files, with constants and wires arguments

  # --- Programmes fournis dans tp1 ---
# "cm2",
# "nadder",
# "clock_div",
# "fulladder",
]

# ----- VARIABLES -----
# Cette variable indique le nombre de tests à effectuer par fichiers.
# S'il y a moins de possibilités que ce nombre, il les essaye toutes.
# Sinon, il en choisit au hasard parmi toutes les entrées possibles.
$input_randomization_threshold = 16
